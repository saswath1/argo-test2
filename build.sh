#!/bin/bash
# List git commits in single line format and pipe to log file
echo "Piping commits to log file"
cd /
apt-get update
apt-get install git -y
git config --global user.email "saswath.v@avasoft.com"
git config --global user.name "saswath"
git clone https://saswath1-admin@bitbucket.org/saswath1/argo-test2.git
cd deploy
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-signupfe-app-depl
  namespace: image-updater-argo
spec:
  selector:
    matchLabels:
      app: signupfe-app
  replicas: 1
  template:
    metadata:
      labels:
        app: signupfe-app
    spec:
      #nodeSelector:
        #zone: US
      containers:
        - name: signupfe-app
          image: $IMAGE_NAME

---
apiVersion: v1
kind: Service
metadata:
  name: my-signupfe-app-srv
  namespace: rishab
spec:
  type: LoadBalancer
  selector:
    app: signupfe-app
  ports:
    - name: my-signupfe-app
      port: 3000
      targetPort: 3000
EOF

git add .
git commit -m "updated deployment.yaml"
git push --force
cd /
rm -rf /argo-test2
